/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on June 4, 2015, 6:51 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

/*
 * 
 */

class Character
{
private:
    string name; //name of the playable character or enemy
    int hp;      //health points in battle
    int str;     //determines attack power in battle
    int def;     //determines resistance to attacks in battle
    int spd;     //determines who goes first in battle
public:
    character();
    character(int, int, int, int); //character constructor
    
    //getters and setters for each stat
    string getName() const {return name;}
    void setName(string n) {name = n;} 
    int getHp() const {return hp;}
    void setHp(int h) {hp = h;}
    int getStr() const {return str;}
    void setStr(int st) {str = st;}
    int getDef() const {return def;}
    void setDef(int d) {def = d;}
    int getSpd() const {return spd;}
    void setSpd(int sp) {spd = sp;}
    void setAll(string n, int h, int st, int d, int sp)
    {
        name = n;
        hp = h;
        str = st;
        def = d;
        spd = sp;
    }
    void getAll() const 
    {
        cout << "Name: " << name << endl;
        cout << "HP: " << hp << endl;
        cout << "Strength: " << str << endl;
        cout << "Defense: " << def << endl;
        cout << "Speed: " << spd << endl;
    }
};

static int love = 0; //determines your relationship with saiko
int fightOne(); //fight function
void clearScreen(); //clears screen
void openingSequence(); //intro code
void saikoPath(); //individual character path
void ending(); //ending scene
int main(int argc, char** argv) 
{
    Character saiko; //player companion
    saiko.setAll("SAIKO", 100, 15, 10, 15); //stats for character, including: name, hp, strength, defense, speed
    int choice; //variable for choosing options within the game
    cout << "GF LAB XD\n"; //starting menu
    cout << "1. New Game\n"; //starts new game
    cin >> choice;
    switch(choice)
    {
        case 1:
            clearScreen();
            openingSequence(); 
            clearScreen();
            saikoPath();
            fightOne(); 
            cout << endl;
            ending();
            break;     
    }
return 0;
}

void clearScreen() 
{
    cout << string(50, '\n'); //couts 50 end lines to simulate a screen clear
}

void openingSequence()
{
    int choice;
    cout << "DR. WOJAK: \n"
            "Hello! Welcome to GF LABS. My name is DR. WOJAK. \n";
    cout << "I'm head researcher here at GF LABS. I'm sure you know \n";
    cout << "why you're here, right?\n";
    cout << "\n 1. No, I don't. Could you please remind me?\n";
    cout << "\n 2. Yes I do. Let's get started.\n";
    cin >> choice;
    clearScreen();
    while (choice != 1 && choice != 2) 
    {
        cout << "DR. WOJAK:\n"
                "Sorry, could you say that again?\n";
        cin >> choice;
        clearScreen();
    }

    if (choice == 1) 
    {
        cout << "DR. WOJAK:\n"
                "I see... Well, you're here because you were \n"
                "randomly selected to beta test our services. \n"
                "We're truly glad you came. We shall begin the tour now.\n";
    }
    if (choice == 2) 
    {
        cout << "DR. WOJAK:\n"
                "Excellent! We shall begin the tour then.\n";
    }
    cout << "\nYOU AND DR. WOJAK BEGIN WALKING DOWN A HALLWAY WITH \n"
            "MANY OLD PICTURES ON THE WALL.\n";
    cout << "\nDR. WOJAK:\n"
            "GF LABS was founded in 1963 shortly after \n"
            "the Cuban Missile Crisis. The government feared that \n"
            "many American women could possibly be wiped out \n"
            "by nuclear warfare, and so they created GF LABS \n"
            "to develop new women to replace those \n"
            "who died in the event of catastrophic nuclear attack. \n"
            "\nFUN FACT: NOT ONLY ARE THESE WOMEN COMPLETELY \n"
            "INDISTINGUISHABLE FROM RUN-OF-THE-MILL NATURAL WOMEN, \n"
            "BUT THEY'VE ALSO BEEN GENETICALLY MODIFIED TO \n"
            "BE RESISTANT TO NUCLEAR RADIATION AS WELL AS MANY \n"
            "OF THE EFFECTS OF NUCLEAR ATTACK. \n";
    cout << "\nDO YOU HAVE ANY OTHER QUESTIONS?\n";
    cout << "\n 1. Why don't you create any men?\n"
            "\n 2. How long have you been head researcher?\n"
            "\n 3. Why does this organization still exist "
            "\n    if the Cold War is over?\n"
            "\n 4. That's it.\n";
    cin >> choice;
    while (choice != 4) 
    {
        clearScreen();
        if (choice == 1) 
        {
            cout << "DR. WOJAK:\n"
                    "We actually did develop genetically modified soldiers \n"
                    "to use in the Vietnam War. However, they were \n"
                    "only marginally better than normal soldiers. \n"
                    "Unfortunately, we had to scrap the project in 1972 \n"
                    "when the Biological Weapons Convention was signed. \n"
                    "It's been over 40 years so I doubt anything \n"
                    "from that project remains now.\n";
            cout << "\nANYTHING ELSE?\n";
            cout << "\n 1. Why don't you create any men?\n"
                    "\n 2. How long have you been head researcher?\n"
                    "\n 3. Why does this organization still exist "
                    "\n    if the Cold War is over?\n"
                    "\n 4. That's it.\n";
            cin >> choice;
        } 
        else if (choice == 2) 
        {
            cout << "DR. WOJAK:\n"
                    "I've been working at GF LABS for 18 years, and\n"
                    "I've been head researcher for 3 years.\n";
            cout << "\nANYTHING ELSE?\n";
            cout << "\n 1. Why don't you create any men?\n"
                    "\n 2. How long have you been head researcher?\n"
                    "\n 3. Why does this organization still exist "
                    "\n    if the Cold War is over?\n"
                    "\n 4. That's it.\n";
            cin >> choice;
        }
        else if (choice == 3) 
        {
            cout << "DR. WOJAK:\n"
                    "We're aiming to commercialize our research and\n"
                    "provide our services to a small albeit very\n"
                    "wealthy market.\n";
            cout << "\nANYTHING ELSE?\n";
            cout << "\n 1. Why don't you create any men?\n"
                    "\n 2. How long have you been head researcher?\n"
                    "\n 3. Why does this organization still exist "
                    "\n    if the Cold War is over?\n"
                    "\n 4. That's it.\n";
            cin >> choice;
        }
    }
    clearScreen();
    cout << "DR. WOJAK:\n"
            "Very well. Let's move on then.\n";
    cout << "\nYOU ENTER A MASSIVE LABORATORY FULL OF COMPLICATED\n"
            "MACHINES AND GIANT TEST TUBES WITH BEAUTIFUL WOMEN INSIDE\n";
    cout << "\nDR. WOJAK:\n"
            "Here is the main facility, the GF laboratory. \n"
            "This is where the growth and development of \n"
            "The GF's happen. This is also where we release GF's \n"
            "after they're ready to harvest. As part of \n"
            "your beta test, we'll allow you to interact with one \n"
            "of the GF's we have on standby. Afterwards, you'll \n"
            "complete a survey detailing you experience at GF LABS. \n"
            "We'll be pairing you with one of our favorites, Saiko.\n"
            "I'll let you do the honors of pulling the lever.\n";
    cout << "\nPRESS ANY KEY TO PULL THE LEVER\n";
    cin >> choice;
}

void saikoPath() 
{
    int choice;
    cout << "COMPUTER:\n";
    cout << "You have chosen Subject #451: Saiko. Please stand by.";
    cout << "\nTHE GREEN FLUID SUSPENDING THE GF WITHIN THE CHAMBER DRAINS\n"
            "AND THE TUBE DETATCHES FROM ITS BASE, RELEASING THE GF.\n"
            "SAIKO STANDS UP AND LOOKS UP YOU...\n";
    cout << "\nSAIKO\n";
    cout << "Tch. What are you lookin' at!?\n";
    cout << "\nDR. WOJAK\n";
    cout << "Now, now, Saiko... Be respectful to our guest.\n"
            "I'll leave you two alone for a bit to chat.\n";
    cout << "\nDR. WOJAK LEAVES THE LABORATORY\n";
    cout << "\nSAIKO\n";
    cout << "So... You wanna help me break out?\n";
    cout << "\n1. WHAT!? Why would I do that?\n"
            "\n2. ...Okay!\n";
    cin >> choice;
    clearScreen();
    if(choice == 1)
    {
        love--;
        cout << "\nSAIKO\n";
        cout << "Come on! Dont be such a wimp!\n";
    }
    else if(choice == 2)
    {
        love++;
        cout << "\nSAIKO\n";
        cout << "Alright! Let's go!\n";
    }
    cout << "\nSAIKO GRABS YOUR HAND TIGHTLY AND PULLS YOU ALONG AS\n"
            "YOU BOTH RUN TOWARD THE EXIT\n";
    cout << "\nDR.WOJAK\n";
    cout << "Where do you think you're going?\n";
    cout << "\nTHE DOOR SLAMS IN FRONT OF YOU BEFORE YOU AND SAIKO CAN ESCAPE.\n"
            "YOU TURN TO SEE DR. WOJAK POINTING A GUN IN YOUR DIRECTION.\n";
    cout << "\nDR.WOJAK\n";
    cout << "GF's are prohibited from leaving the site.\n"
            "Hand her over now before someone gets hurt.\n";
    cout << "\n1. Here! Take her! Just don't shoot!\n"
            "\n2. Make me.\n";
    cin >> choice;
    clearScreen();
    if(choice == 1)
    {
        love--;
        cout << "\nSAIKO\n";
        cout << "As if! I'll die before I go back to that lab!\n";
    }
    else if(choice == 2)
    {
        love++;
        cout << "\nSAIKO\n";
        cout << "Tch. Looks like we're both thinking the same thing.\n";
    }
    cout << "\nDR.WOJAK\n";
    cout << "It looks like you give me no choice...\n\n";    
}

int fightOne()
{
    int dmg; //determines damage output
    int choice;
    string cont; //pseudo-pause
    string movelist = "1. PUNCH\n"
        "2. DODGE\n";
    Character saiko; //calls character
    saiko.setAll("SAIKO", 100, 15, 10, 15); //sets values for saiko
    Character enemy; //calls enemy
    enemy.setAll("DR. WOJAK", 80, 10, 10, 10); //sets values for enemy
    saiko.getAll();
    cout << endl;
    enemy.getAll();
    cout << "\n SELECT A MOVE:\n" << endl;
    cout << movelist; 
    cin >> choice;
    clearScreen();
    
    while(saiko.getHp() >= 0 && enemy.getHp() >= 0) //checks if either character is out of hp
    {        
        if(choice == 1)
        {
            dmg = -(25) + (saiko.getStr() * -2) - (enemy.getDef() * -2); //calculates damage output
            cout << saiko.getName() << " STRUCK " << enemy.getName() << " FOR " 
                    << -(dmg) << " DAMAGE!\n\n";
            enemy.setHp(dmg += enemy.getHp()); //adds damage to hp in order to reduce hp
            if(enemy.getHp() <= 0)
            {
                cout << "YOU HAVE DEFEATED " << enemy.getName() << "!\n";
                return 0;
            }
            else
            {
                saiko.getAll();
                cout << endl;
                enemy.getAll();
                cout << "\n ENEMY TURN\n"
                        "PRESS ANY KEY TO CONINTUE\n";
                cin >> cont;
                clearScreen();
                dmg = -(25) + (enemy.getStr() * -2) - (saiko.getDef() * -2);
                cout << enemy.getName() << " SHOT " << saiko.getName() <<
                        " FOR " << -(dmg) << " DAMAGE!\n\n";
                saiko.setHp(dmg += saiko.getHp());
                if(saiko.getHp() <= 0)
                {
                    cout << "YOU SEE YOUR GF LYING STILL ON THE GROUND.\n"
                            "HER BODY BEGINS TO RUMBLE VIOLENTLY.\n"
                            "A CHILL GROWS IN THE AIR AS HER BODY ABSORBS\n"
                            "ALL OF THE HEAT IN THE AREA.\n"
                            "\nA SPATIAL VORTEX ERUPTS IN THE SPOT WHERE\n"
                            "YOUR GIRLFRIEND ONCE STOOD.\n";
                    cout << "PRESS ANY KEY TO SUCCUMB TO THE VORTEX\n";
                    cin >> cont;
                    clearScreen();
                    return 1;
                }
                saiko.getAll();
                cout << endl;
                enemy.getAll();
                cout << "\n SELECT A MOVE:\n" << endl;
                cout << movelist;
                cin >> choice;
                clearScreen();
            }
        }
        else if(choice == 2)
        {
            cout << saiko.getName() << " ENTERS AN EVASIVE STANCE\n\n";
            saiko.getAll();
            cout << endl;
            enemy.getAll();
            cout << "\n ENEMY TURN\n"
                    "PRESS ANY KEY TO CONINTUE\n";
            cin >> cont;
            clearScreen();
            cout << enemy.getName() << " TRIES TO SHOOT " << saiko.getName() <<
                    " BUT " << saiko.getName() << " DODGES!\n\n";
            saiko.getAll();
            cout << endl;
            enemy.getAll();
            cout << "\n SELECT A MOVE:\n" << endl;
            cout << movelist;
            cin >> choice;
            clearScreen();
        }
    }
    return 0;    
}

void ending()
{
    int choice;
    cout << "\n DR. WOJAK LETS OUT A PAINFUL CRY AND FALLS TO THE FLOOR\n";
    cout << "\nSAIKO:\n"
            "Phew... well, thanks for the help, I guess\n";
    cout << "Hey... Ummm... Look, I know we just met, but you seem like a pretty cool guy, and..\n."
                  "we should ummm... tch- never mind. You'll probably say no anyway.\n";
    cout << "\n1. No, really, it's alright. Please tell me.\n"
                  "\n2. Okay.\n";
    cin >> choice;
    clearScreen();
        if(choice == 2)
        {
            love =0;
            cout << "Love: " << love;
        }
        cout << "\nSAIKO:\n";
        cout << "Well... You're pretty brave, and I like that. Let's get to know each other better.\n\n";
        cout << "1. Let's do it.\n"
                  "\n2. I think I'll pass...\n";
        cin >> choice;
        clearScreen();
        if(choice == 2)
        {
            love = 0;
        }
        cout << "YOU AND SAIKO LEAVE THE LABORATORY HAND-IN-HAND.\n"
                "SHORTLY AFTER, YOU AND SAIKO GET MARRIED, MOVE TO A QUAINT HOME\n"
                "IN TENNESSEE, AND HAVE TWO CHILDREN.\n"
                "\nPRESS ANY KEY THEN ENTER TO CONTINUE.\n";
        cin >> choice;
        clearScreen();
        cout << "Thank you for playing.";

    cout << "\nTch- Well, see you later I guess.\n";
    cout << "\nSAIKO GIVES YOU A ROUGH PAT ON  THE BACK AND RUNS OFF.\n"
            "\nYOU LEAVE THE GF LABORATORY BUILDING FEELING EMPTY INSIDE, BUT\n"
            "YOU CAN'T QUITE FIGURE OUT WHY. YOU GO BACK HOME AND WATCH ANIME\n"
            "ALL DAY LIKE YOU ALWAYS HAVE, AND LIFE NEVER CHANGES.\n";
     cout << "\nYOU NEVER HEAR FROM SAIKO AGAIN.\n";
     cout << "\nPRESS ANY KEY THEN ENTER TO CONTINUE.\n";
     cin >> choice;
     clearScreen();
     cout << "Thank you for playing.";    
}

