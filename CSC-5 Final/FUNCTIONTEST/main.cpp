/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on June 5, 2015, 6:00 PM
 */

#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;

/*
 * 
 */

class Character
{
private:
    string name; //name of the playable character or enemy
    int hp;      //health points in battle
    int str;     //determines attack power in battle
    int def;     //determines resistance to attacks in battle
    int spd;     //determines who goes first in battle
public:
    character();
    character(int, int, int, int);
    string getName() const {return name;}
    void setName(string n) {name = n;} 
    int getHp() const {return hp;}
    void setHp(int h) {hp = h;}
    int getStr() const {return str;}
    void setStr(int st) {str = st;}
    int getDef() const {return def;}
    void setDef(int d) {def = d;}
    int getSpd() const {return spd;}
    void setSpd(int sp) {spd = sp;}
    void setAll(string n, int h, int st, int d, int sp)
    {
        name = n;
        hp = h;
        str = st;
        def = d;
        spd = sp;
    }
    void getAll() const 
    {
        cout << "Name: " << name << endl;
        cout << "HP: " << hp << endl;
        cout << "Strength: " << str << endl;
        cout << "Defense: " << def << endl;
        cout << "Speed: " << spd << endl;
    }
};

void clearScreen()
{
    cout << string(50, '\n');
}

int fightOne()
{
    int dmg;
    int choice;
    string cont;
    string movelist = "1. PUNCH\n"
        "2. DODGE\n";
    Character saiko;
    saiko.setAll("SAIKO", 100, 15, 10, 15);
    Character enemy;
    enemy.setAll("DR. WOJAK", 80, 10, 10, 10);
    saiko.getAll();
    cout << endl;
    enemy.getAll();
    cout << "\n SELECT A MOVE:\n" << endl;
    cout << movelist;
    cin >> choice;
    clearScreen();
    
    while(saiko.getHp() >= 0 && enemy.getHp() >= 0)
    {        
        if(choice == 1)
        {
            dmg = -(25) + (saiko.getStr() * -2) - (enemy.getDef() * -2);
            cout << saiko.getName() << " STRUCK " << enemy.getName() << " FOR " 
                    << -(dmg) << " DAMAGE!\n\n";
            enemy.setHp(dmg += enemy.getHp());
            if(enemy.getHp() <= 0)
            {
                cout << "YOU HAVE DEFEATED " << enemy.getName() << "!\n";
                return 0;
            }
            else
            {
                saiko.getAll();
                cout << endl;
                enemy.getAll();
                cout << "\n ENEMY TURN\n"
                        "PRESS ANY KEY TO CONINTUE\n";
                cin >> cont;
                clearScreen();
                dmg = -(25) + (enemy.getStr() * -2) - (saiko.getDef() * -2);
                cout << enemy.getName() << " STRUCK " << saiko.getName() <<
                        " FOR " << -(dmg) << " DAMAGE!\n\n";
                saiko.setHp(dmg += saiko.getHp());
                if(saiko.getHp() <= 0)
                {
                    cout << "YOU SEE YOUR GF LYING STILL ON THE GROUND.\n"
                            "HER BODY BEGINS TO RUMBLE VIOLENTLY.\n"
                            "A CHILL GROWS IN THE AIR AS HER BODY ABSORBS\n"
                            "ALL OF THE HEAT IN THE AREA.\n"
                            "\nA SPATIAL VORTEX ERUPTS IN THE SPOT WHERE\n"
                            "YOUR GIRLFRIEND ONCE STOOD.\n";
                    cout << "PRESS ANY KEY TO SUCCUMB TO THE VORTEX\n";
                    cin >> cont;
                    clearScreen();
                    return 1;
                }
                saiko.getAll();
                cout << endl;
                enemy.getAll();
                cout << "\n SELECT A MOVE:\n" << endl;
                cout << movelist;
                cin >> choice;
                clearScreen();
            }
        }
        else if(choice == 2)
        {
            cout << saiko.getName() << " ENTERS AN EVASIVE STANCE\n\n";
            saiko.getAll();
            cout << endl;
            enemy.getAll();
            cout << "\n ENEMY TURN\n"
                    "PRESS ANY KEY TO CONINTUE\n";
            cin >> cont;
            clearScreen();
            cout << enemy.getName() << " STRIKES " << saiko.getName() <<
                    " BUT " << saiko.getName() << " DODGES!\n\n";
            saiko.getAll();
            cout << endl;
            enemy.getAll();
            cout << "\n SELECT A MOVE:\n" << endl;
            cout << movelist;
            cin >> choice;
            clearScreen();
        }
    }    
    return 0;    
}

int main(int argc, char** argv) 
{

    fightOne();
    cout << "Success.";
    return 0;
}

