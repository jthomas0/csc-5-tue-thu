#include<SDL.h>
#include<SDL_image.h>
#include<iostream>


SDL_Texture *LoadTexture(std::string filePath, SDL_Renderer *renderTarget)
{
	SDL_Texture *texture = NULL;
	SDL_Surface *surface = IMG_Load(filePath.c_str());
	if (surface == NULL)
		std::cout << "Error" << std::endl;
	else
	{
		texture = SDL_CreateTextureFromSurface(renderTarget, surface);
		if (texture == NULL)
			std::cout << "Error" << std::endl;
	}

	SDL_FreeSurface(surface);

	return texture;
}

int main(int argc, char *argv[])
{
	// Initializing and loading variables
	SDL_Window *window = NULL; //window that objects and images will load to
	SDL_Surface *windowSurface = NULL; //loads surface on window for images to be displayed onto
	SDL_Texture *bg = NULL; //background texture loader
	SDL_Texture *currentImage = NULL; //sets default value for texture
	SDL_Renderer *renderTarget = NULL; //default value for renderer
	SDL_Rect paddle1Rect; //declaring object "paddle1"
	SDL_Rect paddle1Position; //value for determining position of paddle1
	paddle1Position.x = 320; //x position of paddle1
	paddle1Position.y = 400; //y position of paddle1 
	paddle1Position.w = paddle1Position.h = 32; //width and height of paddle1
	SDL_Rect paddle2Rect;
	SDL_Rect paddle2Position;
	paddle2Position.x = 352;
	paddle2Position.y = 400;
	paddle2Position.w = paddle2Position.h = 32;
	SDL_Rect paddle3Rect;
	SDL_Rect paddle3Position;
	paddle3Position.x = 282;
	paddle3Position.y = 400;
	paddle3Position.w = paddle3Position.h = 32;
	int frameWidth, frameHeight; //for selecting specific sprites from sprite sheet
	int textureWidth, textureHeight; //size of original image
	float frameTime = 0; //default framerate value
	int prevTime = 0; //time before change
	int currentTime = 0; //time currently occurring
	float deltaTime = 0; //change in time (prevTime - currentTime)
	float moveSpeed = 210.0f; //speed of paddle movement
	const Uint8 *keyState; //for deterining key presses

	//initializer
	SDL_Init(SDL_INIT_VIDEO);

	//calling to allow .png and .jpg images
	int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
	if (IMG_Init(imgFlags) != imgFlags)
		std::cout << "Error: " << IMG_GetError() << std::endl;

	//creating window and setting window values
	window = SDL_CreateWindow("Appel Toss Championship Edition", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);
	renderTarget = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	bg = LoadTexture("appeltitel.bmp", renderTarget); //loading background to window
	currentImage = LoadTexture("image.png", renderTarget); //loading paddle texture

	SDL_QueryTexture(currentImage, NULL, NULL, &textureWidth, &textureHeight);

	//dividing size of original image to suit sprites accordingly
	frameWidth = textureWidth / 3;
	frameHeight = textureHeight / 4;

	//crops needed section from original image

	paddle1Rect.x = paddle1Rect.y = 0;
	paddle1Rect.w = frameWidth;
	paddle1Rect.h = frameHeight;

	paddle2Rect.x = paddle2Rect.y = 0;
	paddle2Rect.w = frameWidth;
	paddle2Rect.h = frameHeight;

	paddle3Rect.x = paddle3Rect.y = 0;
	paddle3Rect.w = frameWidth;
	paddle3Rect.h = frameHeight;

	SDL_SetRenderDrawColor(renderTarget, 188, 235, 215, 0xFF); //sets background color to pastel blue

	bool isRunning = true; //checks to see if the program is running
	SDL_Event event;

	while (isRunning)
	{
		prevTime = currentTime;
		currentTime = SDL_GetTicks(); //retrieves number of milliseconds passed
		deltaTime = (currentTime - prevTime) / 1000.0f; //calculates change in time
		while (SDL_PollEvent(&event) != 0)
		{
			// Getting the events
			if (event.type == SDL_QUIT)
				isRunning = false;
		}

		keyState = SDL_GetKeyboardState(NULL);
		if (keyState[SDL_SCANCODE_RIGHT]) //moves paddle right
		{
			paddle1Position.x += moveSpeed * deltaTime;
			paddle2Position.x += moveSpeed * deltaTime;
			paddle3Position.x += moveSpeed * deltaTime;
		}
		else if (keyState[SDL_SCANCODE_LEFT]) //moves paddle left
		{
			paddle1Position.x -= moveSpeed * deltaTime;
			paddle2Position.x -= moveSpeed * deltaTime;
			paddle3Position.x -= moveSpeed * deltaTime;
		}
		frameTime += deltaTime;

		if (frameTime >= 0.25f) //fixes framerate and makes game run the same across all platforms
								//animates sprite by alternating between images synchronizd with framerate
		{
			frameTime = 0;
			paddle1Rect.x += frameWidth;
			paddle2Rect.x += frameWidth;
			paddle3Rect.x += frameWidth;
			if (paddle1Rect.x >= textureWidth)
				paddle1Rect.x = 0;
			if (paddle2Rect.x >= textureWidth)
				paddle2Rect.x = 0;
			if (paddle3Rect.x >= textureWidth)
				paddle3Rect.x = 0;
		}

		SDL_RenderClear(renderTarget);
		SDL_RenderCopy(renderTarget, currentImage, &paddle1Rect, &paddle1Position);
		SDL_RenderCopy(renderTarget, currentImage, &paddle2Rect, &paddle2Position);
		SDL_RenderCopy(renderTarget, currentImage, &paddle3Rect, &paddle3Position);
		SDL_RenderPresent(renderTarget);
	}

	//destroy objects and set all pointers to null
	SDL_DestroyWindow(window);
	SDL_DestroyTexture(currentImage);
	SDL_DestroyRenderer(renderTarget);
	window = NULL;
	windowSurface = NULL;
	bg = NULL;
	currentImage = NULL;
	renderTarget = NULL;

	SDL_Quit(); //quit SDL

	return 0;
}