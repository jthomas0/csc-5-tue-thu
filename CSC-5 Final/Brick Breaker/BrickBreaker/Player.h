#pragma once
#include<SDL.h>
#include<string>
class Block
{
private: 
	SDL_Rect cropRect;
	float moveSpeed;
	float frameCounter;
	int frameWidth;
	int frameHeight;
	int textureWidth;
	bool isActive;
public:
	Player(SDL_Renderer *renderTarget, std::string filePath,
		int x, int y, int framesX, int framesY);
	~Player(;

	void Update(float data, const Uint8 *keyState);
	void Draw(SDL_Renderer *renderTarget);

	SDL_Rect positionRect;
};

