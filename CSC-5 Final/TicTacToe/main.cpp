/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on June 3, 2015, 8:29 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

char board[3][3] = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
void printBoard();
void inputMove();
void isPlayerOne();
void playerVsPlayer();
int turnCount = 0;
int main(int argc, char** argv) 
{
    int num;
    cout << "TIC TAC TOE\n";
    
    cout << "\n1. Player vs Player game\n"
            "2. Player vs Easy AI\n"
            "3. Player vs Hard AI\n"
            "4. Easy AI vs Easy AI\n"
            "5. Options\n";
    cout << "\n Enter a number to select a mode: ";
    cin >> num;
    switch(num)
    {
       case 1:
           
           printBoard();
           playerVsPlayer();
           break;
        //case 2:
            //playerVsDumbAi();
           // break;
        //case 3:
            //playerVsSmartAi();
            //break;
       // case 4:
           //dumbAiVsDumbAi();
            //break;
    }

    return 0;
}

void inputMove(char board[3][3], int num1, int num2)
{
    if(isPlayerOne == true)
    {
        board[num1][num2] = 'X';
    }
    else
    {
        board[num1][num2] = 'O';
    }
}

bool isPlayerOne()
{
    if(turnCount % 2 == 0)
    {
        return true;
    }
}

void printBoard()
{
    cout << "\n ________________\n";
    cout << "|     |     |     |\n";
    cout << "|     |     |     |\n";
    cout << "|_____|_____|_____|\n";
    cout << "|     |     |     |\n";
    cout << "|     |     |     |\n";
    cout << "|_____|_____|_____|\n";
    cout << "|     |     |     |" << endl;
    cout << "|     |     |     |\n";
    cout << "|_____|_____|_____|\n";   
}

void playerVsPlayer()
{
    int num1, num2;
    cout << "Player 1 Turn.\n"
            "Enter row number > ";
    cin >> num1;
    cout << "\nEnter column number > ";
    cin >> num2;
    isPlayerOne();
    inputMove(board, num1, num2);
}
void playerVsDumbAi()
{

}
void playerVsSmartAi()
{
    
}
void dumbAiVsDumbAi()
{
    
}