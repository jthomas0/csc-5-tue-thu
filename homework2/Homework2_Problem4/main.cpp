/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on March 10, 2015, 11:21 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    string name1, name2, food, adj, color, animal;
    int num;
    cout << "Please enter the following: " << endl;
    cout << "A name: ";
    cin >> name1;
    cout << endl << "Another name: ";
    cin >> name2;
    cout << endl << "A food: ";
    cin >> food;
    cout << endl << "A number between 100 and 200: ";
    cin >> num;
    cout << endl << "An adjective: ";
    cin >> adj;
    cout << endl << "A color: ";
    cin >> color;
    cout << endl << "An animal: ";
    cin >> animal;
    cout << "Thank you." << endl;
    cout << "Dear, " << name1 << endl;
    cout << "I am sorry that I am unable to turn in my homework at this time."
         << " First, I ate a rotten " << food << ", which made me turn " << endl
         << color << " and extremely ill. " << "I came down with a " << endl 
         << "fever of " << num << ". Next, my " << adj << " pet " << animal <<
            endl << "must have smelled the remains of the " << food << endl
         << "on my homework because he ate it." << "I am currently " << endl
         << "rewriting my homework and I hope you will accept it late." << endl
         << endl << "Sincerely, " << endl << name2;
    return 0;
}

