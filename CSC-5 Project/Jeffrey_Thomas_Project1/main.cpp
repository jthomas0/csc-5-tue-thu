/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on April 12, 2015, 9:19 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

int roulette() 
{
    int win;
    srand(time(0));
    win = rand() % 37;
    return win;
}

int diceRoll1() 
{
    int num1, num2;
    srand(time_t(1));
    num1 = rand() % 6 + 1;
    srand(time(0));
    num2 = rand() % 6 + 1;
    int roll = num1 + num2;
    return roll;
}

int diceRoll2() 
{
    srand(time(NULL));
    int num1, num2;

    num1 = rand() % 6 + 1;
    srand(time(0));
    num2 = rand() % 6 + 1;
    int roll = num1 + num2;
    return roll;
}

void clearScreen() 
{
    for (int i = 0; i < 50; i++)
        cout << endl;
}

int main(int argc, char** argv) 
{
    int choice, num, wager, chips, minBet;
    string play;
    chips = 100;
    do {
        cout << chips << " chip(s)\n";
        cout << "                       THE CASINO\n";
        cout << "Welcome back, sir. Which game would you like to play?\n";
        cout << "1. Craps\n";
        cout << "2. Roulette\n";
        cout << "3. Leave The Casino\n";
        cin >> num;
        clearScreen();
        switch (num) 
        {
            case 1:
                cout << "Craps\n";
                cout << "Place your bet: ";
                cin >> wager;
                clearScreen();
                if (wager > chips) 
                {
                    cout << "INSUFFICIENT CHIPS";
                } 
                else 
                {
                    cout << "Current # of chips: " << chips << ", ";
                    cout << "Current bet: " << wager << endl;
                    cout << "First roll: " << diceRoll1();
                    int roll1 = diceRoll1();
                    if (roll1 == 7 && 11) 
                    {
                        cout << endl << "You win!\n";
                        chips += wager;
                        cout << "Chips earned: " << wager << endl;
                        cout << "Total chips: " << chips << endl;
                        cout << "Play again?\n";
                        cin >> play;
                        clearScreen();
                    } 
                    else if (roll1 == 2 && 3 && 12) 
                    {
                        cout << endl << "You lose!";
                        chips -= wager;
                        cout << "Chips lost: " << wager << endl;
                        cout << "Total chips: " << chips << endl;
                        cout << "Play again?\n";
                        cin >> play;
                        clearScreen();
                    }
                    else 
                    {
                        cout << endl << "Try again!" << endl;
                        cout << "Second roll: " << diceRoll2() << endl;
                        int roll2 = diceRoll2();
                        do {
                            cout << "Try again!\n";
                            cout << roll2 << endl;
                        } 
                        while (roll2 == 7 && roll2 == roll1);
                        if (roll2 == 7) 
                        {
                            cout << "You lose!\n";
                            chips -= wager;
                            cout << "Chips lost: " << wager << endl;
                            cout << "Total chips: " << chips << endl;
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                        } 
                        else 
                        {
                            cout << "You win!\n";
                            chips += wager;
                            cout << "Chips earned: " << wager << endl;
                            cout << "Total chips: " << chips << endl;
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                        }
                    }
                }
                break;
            case 2:
                minBet = chips;
                cout << "Place your bet: ";
                cin >> wager;
                clearScreen();
                cout << "Current Bet: " << wager << endl << endl;
                if (wager > minBet) 
                {
                    cout << "Insufficient Chips";
                } 
                else 
                {
                    cout << "What would you like to bet on?" << endl;
                    cout << "1. All Even Numbers (1:1)" << endl;
                    cout << "2. All Odd Numbers (1:1)" << endl;
                    cout << "3. All Red Numbers (1:1)" << endl;
                    cout << "4. All Black Numbers (1:1)" << endl;
                    cout << "5. First Half (1:1)" << endl;
                    cout << "6. Second half (1:1)" << endl;
                    cout << "7. Numbers 1-12 (2:1)" << endl;
                    cout << "8. Numbers 13-24 (2:1)" << endl;
                    cout << "9. Numbers 25-36 (2:1)" << endl;
                    cout << "10. One Column (2:1)" << endl;
                    cout << "11. One Row (5:1)" << endl;
                    cout << "12. One Number (35:1)" << endl;
                    cin >> num;
                    clearScreen();
                    switch (num) 
                    {
                        case 1:
                            roulette();
                            if (roulette() % 2 == 0) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << endl << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 2:
                            roulette();
                            if (roulette() % 2 == 1) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 3:
                            roulette();
                            if (roulette() < 10 && roulette() % 2 == 1) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 9 && 
                                    roulette() < 19 && roulette() % 2 == 0) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 20 && 
                                    roulette() < 29 && roulette() % 2 == 1) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 29 && 
                                    roulette() < 37 && roulette() % 2 == 0) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 4:
                            roulette();
                            if (roulette() < 11 && roulette() % 2 == 0) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 10 && 
                                    roulette() < 19 && roulette() % 2 == 1) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 19 && 
                                    roulette() < 29 && roulette() % 2 == 0) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else if
                                (roulette() > 28 && 
                                    roulette() < 37 && roulette() % 2 == 1) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen;
                            break;
                        case 5:
                            roulette();
                            if (roulette() < 19) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 6:
                            roulette();
                            if (roulette() > 18) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 7:
                            roulette();
                            if (roulette() < 13) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen;
                            break;
                        case 8:
                            roulette();
                            if (roulette() > 12 && roulette() < 25) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 9:
                            roulette();
                            if (roulette() > 24) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 10:
                            cout << "Please select a column (1-3): ";
                            cin >> choice;
                            roulette();
                            if (choice < 3) {
                                if (roulette() % 3 == choice) 
                                {
                                    cout << "Roll " << 
                                            roulette() << ". WIN!\n";
                                } 
                                else 
                                {
                                    cout << "Roll " << 
                                            roulette() << ". LOSE!\n";
                                }
                            }
                            else 
                            {
                                if (roulette() % 3 == choice - 3)
                                {
                                    cout << "Roll " << 
                                            roulette() << ". WIN!\n";
                                } 
                                else
                                {
                                    cout << "Roll " << 
                                            roulette() << ". LOSE!\n";
                                }
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 11:
                            cout << "Please select a row: ";
                            cin >> choice;
                            roulette();
                            if (roulette() >= choice * 3 - 2 && 
                                    roulette() <= choice * 3) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }

                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << "Play again?\n";
                            cin >> play;
                            clearScreen();
                            break;
                        case 12:
                            cout << "Please select a number: ";
                            cin >> choice;
                            roulette();
                            if (roulette() == choice) 
                            {
                                cout << "Roll " << roulette() << ". WIN!\n";
                                chips += wager;
                                cout << "Chips earned: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            } 
                            else 
                            {
                                cout << "Roll " << roulette() << ". LOSE!\n";
                                chips -= wager;
                                cout << "Chips lost: " << wager << endl;
                                cout << "Total chips: " << chips << endl;
                            }
                            cout << endl << "Play again?\n";
                            cin >> play;
                            clearScreen();

                            break;
                    }
                }

                break;
            case 3:
                cout << "Please stay... (yes/no)\n";
                cin >> play;
                break;
        }
    } 
    while (play != "no" && "NO" && "No" && "nO" && "n" && "N");

    return 0;
}