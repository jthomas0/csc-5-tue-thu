/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on March 19, 2015, 10:09 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int max, attend;
    cout << "Please enter maximum occupancy of meeting room: ";
    cin >> max;
    cout << "Please enter number of people attending: ";
    cin >> attend;
    if(max <= attend)
    {
        cout << endl << "Number of occupants exceeds maximum occupancy. " 
             << endl << "Meeting will be canceled due to violation of fire"
             << endl << "regulations.";
    }
    else
    {
        cout << endl << "Meeting will continue as planned.";
    }
    return 0;
}

