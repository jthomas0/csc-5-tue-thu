/* 
 * File:   main.cpp
 * Author: Jeff
 *
 * Created on March 19, 2015, 10:53 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int val, num, sum = 0;
 
    cout << "Enter the number: ";
    cin >> val;
    num = val;
    while (num != 0)
    {
        sum = sum + num % 10;
        num = num / 10;
    }
    cout << "The sum of the digits of "
         << val << " is " << sum;
    return 0;
}

